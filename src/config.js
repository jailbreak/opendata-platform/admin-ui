function withoutTrailingSlash(s) {
	return s ? s.replace(/\/$/, "") : null
}

export function buildConfigFromEnv() {
	// Builds a config object that is intended to be run on server side.
	// Client side will receive config via Sapper session store.

	const baseUrl = withoutTrailingSlash(process.env.BASE_URL)
	if (!baseUrl) {
		throw Error(`Undefined environment variable: BASE_URL`)
	}

	const catalogProjectName = process.env.CATALOG_PROJECT_NAME
	if (!catalogProjectName) {
		throw Error(`Undefined environment variable: CATALOG_PROJECT_NAME`)
	}

	const gitlabBaseUrl = withoutTrailingSlash(process.env.GITLAB_BASE_URL)
	if (!gitlabBaseUrl) {
		throw Error(`Undefined environment variable: GITLAB_BASE_URL`)
	}

	const gitlabApplicationId = process.env.GITLAB_APPLICATION_ID
	if (!gitlabApplicationId) {
		throw Error(`Undefined environment variable: GITLAB_APPLICATION_ID`)
	}

	const gitlabApplicationSecret = process.env.GITLAB_APPLICATION_SECRET
	if (!gitlabApplicationSecret) {
		throw Error(`Undefined environment variable: GITLAB_APPLICATION_SECRET`)
	}

	const sessionSecret = process.env.SESSION_SECRET
	if (!sessionSecret) {
		throw Error(`Undefined environment variable: SESSION_SECRET`)
	}

	return { baseUrl, catalogProjectName, gitlabBaseUrl, gitlabApplicationId, gitlabApplicationSecret, sessionSecret }
}
