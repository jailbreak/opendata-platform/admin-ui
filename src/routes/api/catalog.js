import { Gitlab } from "gitlab"

/**
 * Respond a JSON representation of a GitLab project corresponding to the Data Package catalog of the authenticated user.
 * @param {*} req Request
 * @param {*} res Response
 */
export async function get(req, res) {
	const { config, user } = req
	if (!user) {
		return res.status(401).json({ err: "Unauthorized" })
	}

	const { Projects } = new Gitlab({
		host: config.gitlabBaseUrl,
		oauthToken: user.accessToken
	})

	const projects = await Projects.search(user.catalogProjectName)

	if (projects.length > 1) {
		return res
			.status(500)
			.json({ err: `More than one GitLab project matches "${catalogProjectName}", but one was expected.` })
	}

	if (projects.length === 0) {
		res.status(404)
	}

	const project = projects.length > 0 ? projects[0] : null

	return res.json({ project })
}
