import { Gitlab } from "gitlab"

/**
 * Create a new GitLab repository for a Data Package, commit the posted resources and add it to the Data Package catalog repository of the authenticated user.
 * @param {*} req Request
 * @param {*} res Response
 */
export async function post(req, res) {
	const { config, user } = req
	if (!user) {
		return res.status(401).json({ err: "Unauthorized" })
	}

	const { fields, files } = req

	const { name } = fields

	const { Projects } = new Gitlab({
		host: config.gitlabBaseUrl,
		oauthToken: user.accessToken
	})

	let project
	try {
		// create a new GitLab repository for a Data Package
		project = await Projects.create({ name, visibility: "public" })
	} catch (error) {
		// Manually catching error to log it as Sapper doesn't do it for now
		console.error(error)
		return res.status(400).json({ err: error })
	}

	// commit the posted resources
	console.log({ files })

	// add it to the Data Package catalog repository
	res.json({ project })
}
