/**
 * Redirect to login page if not authenticated.
 * Use in Sapper preload function of routes requiring authentication.
 * @param {Object} page identical to Sapper preload function
 * @param {Object} session identical to Sapper preload function
 */
export function ensureAuthenticated(page, session) {
	const { path } = page
	const { user } = session
	if (!user) {
		return this.redirect(302, `/login?returnTo=${path}`)
	}
}
