import sirv from "sirv"
import passport from "passport"
import { Strategy as GitLabStrategy } from "passport-gitlab2"
import express from "express"
import session from "express-session"
import SessionFileStore from "session-file-store"
import querystring from "querystring"
import compression from "compression"
import formidableMiddleware from "express-formidable"
import * as sapper from "@sapper/server"
import url from "url"
import { buildConfigFromEnv } from "./config.js"

const config = buildConfigFromEnv()

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const FileStore = SessionFileStore(session)

passport.use(
	new GitLabStrategy(
		{
			clientID: config.gitlabApplicationId,
			clientSecret: config.gitlabApplicationSecret,
			callbackURL: `${config.baseUrl}/auth/gitlab/callback`,
			baseURL: config.gitlabBaseUrl
		},
		function(accessToken, refreshToken, profile, cb) {
			return cb(null, { ...profile, accessToken, refreshToken })
		}
	)
)

/**
 * Add default properties to user session.
 *
 * Mutates user parameter, does not return anything.
 *
 * @param {Object} user
 */
function setSessionDefaultProperties(user) {
	if (!user.catalogProjectName) {
		user.catalogProjectName = config.catalogProjectName
	}
	if (!user.catalogProjectPath) {
		user.catalogProjectPath = `${user.username}/${config.catalogProjectName}`
	}
}

passport.serializeUser(function(user, cb) {
	setSessionDefaultProperties(user)
	cb(null, user)
})

passport.deserializeUser(function(user, cb) {
	setSessionDefaultProperties(user)
	cb(null, user)
})

// The middleware to set up the parameters for the authenticate middleware.
// From https://gist.github.com/tchap/5643644
function checkReturnTo(req, res, next) {
	var returnTo = req.query["returnTo"]
	if (returnTo) {
		// Maybe unnecessary, but just to be sure.
		req.session = req.session || {}

		// Set returnTo to the path you want to be redirect to after the authentication succeeds.
		req.session.returnTo = querystring.unescape(returnTo)
	}
	next()
}

express()
	.use(
		formidableMiddleware(),
		session({
			resave: false,
			saveUninitialized: false,
			secret: config.sessionSecret,
			store: new FileStore()
		}),
		passport.initialize(),
		passport.session(),
		function(req, res, next) {
			req.config = config
			next()
		},
		compression({ threshold: 0 }),
		sirv("static", { dev }),
		sapper.middleware({
			ignore: [/^\/auth.+/, "/login", "/logout"], // otherwise ".get" calls below don't get called
			session: (req, res) => {
				// Send only certain keys to the client (not the secrets).
				const { baseUrl, gitlabBaseUrl } = config
				return {
					config: { baseUrl, gitlabBaseUrl },
					user: req.user
				}
			}
		})
	)

	// use routes/login.html to let the user choose an identity provider among many
	.get("/login", (req, res) => {
		let path = "/auth/gitlab"
		const qs = url.parse(req.url).query
		if (qs) {
			path += `?${qs}`
		}
		res.redirect(path)
	})
	.get(
		"/auth/gitlab",
		checkReturnTo,
		passport.authenticate("gitlab", {
			scope: ["api"]
		})
	)
	.get(
		"/auth/gitlab/callback",
		passport.authenticate("gitlab", {
			successReturnToOrRedirect: "/",
			failureRedirect: "/error/account-not-found"
		})
	)
	.get("/logout", function(req, res) {
		req.logout()
		res.redirect("/")
	})

	.listen(PORT, err => {
		if (err) console.log("error", err)
	})
