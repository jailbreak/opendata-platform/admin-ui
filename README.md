# Open Data Platform – Admin UI

https://forum.jailbreak.paris/t/design-projet-plateforme-open-data-jailbreak/841

## Development

Install:

```bash
git clone https://framagit.org/jailbreak/opendata-platform/admin-ui.git
cd admin-ui
npm install
```

Configure:

```bash
cp .env.template .env
```

Edit `.env` and fill-in empty values (especially `GITLAB_APPLICATION_ID` and `GITLAB_APPLICATION_SECRET`). See comments in `.env` file for details.

Run:

```bash
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.

## See also

- https://frictionlessdata.io/specs/data-package/
